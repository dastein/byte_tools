/*    This program takes a text-file and creates a corresponding bin-file which
 *    contains all 1s and 0s from the text-file in byte form.
 *
 *    -------------------------------------------------------------------------
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *    (C) Philipp Stanner, <stanner@posteo.de>
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>

#define VALID_CHAR(c) (c == '0' || c == '1')


char* substr(char *in, int pos)
{
	char *out = malloc(pos+2);

	if (out) {
		memcpy(out, in, pos+1);
		out[pos+1] = '\0';
	}

	return out;
}


char* prepare_filename(char *namein)
{
	char *nameout = NULL, *sub = NULL;
	
	/* Filenames don't exceed 255 characters on Unixoids */
	int pos = 0;
	int sl = strlen(namein);

	for (pos=0; pos < sl && namein[pos] != '.'; pos++);

	if (namein[pos] == '.') {
		sub = substr(namein, pos);
		if (!sub)
			return NULL;

		if (asprintf(&nameout, "%sbin", sub) == -1) {
			perror("prepare_filename");
			nameout = NULL;
		}

		free(sub);
	} else if (asprintf(&nameout, "%s.bin", namein) == -1) {
			perror("perpare_filename");
			nameout = NULL;
	}
	
	return nameout;
}


/* This function should never get chars other than 1 and 0 */
uint8_t chars_to_byte(char *arr)
{
	uint8_t ret = 0x00;
	int i;

	for (i=7; i>=0; i--) {
		if (arr[i] == '1')
			ret |= 1UL<<i;
		else
			ret &= ~(1UL<<i);
	}

	return ret;
}


int write_byte(FILE *fp, uint8_t byte)
{
	int ret = fwrite(&byte, 1, 1, fp);
	if (ret != 1)
		perror("write_byte");

	return ret;
}


/* ret-value indicates to main how many unused bits are in the last byte */
int parse_and_write(FILE *fin, FILE *fout, bool zeros)
{
	int c, i=0, j, filler='0';
	char writearr[8];
	uint8_t byte = 0x00;

	if (!zeros)
		filler = '1';

	/* Initialize c. This way we avoid fgetc() in loop-heads, what seems to
	 * cause awkward problems. */
	c = fgetc(fin);
	while (c != EOF) {
		 i=0;
		 while (i<8 && c != EOF) {
			 if (VALID_CHAR(c)) {
				 writearr[i] = (char)c;
				 i++;
			 }
			 c = fgetc(fin);
		}
		 
		if (i == 8) {
			byte = chars_to_byte(writearr);
			if (write_byte(fout, byte) != 1)
				return -1;
		}
	}

	if (i > 0) {
		fprintf(stderr, "Last Byte incomplete. Filling %i Bits with %c\n",
				8-i, filler);

		/* Set remaining bits to 'filler' */
		for (j=i; j < 8; j++) {
			writearr[j] = filler;
		}

		byte = chars_to_byte(writearr);
		if (write_byte(fout, byte) != 1)
			return -1;
	}

	return 8-i;
}


int decorate_end(FILE *fp, int rem)
{
	int ret = EOF;
	char str[6];

	if (snprintf(str, 6, "END-%1i", rem) < 0)
		perror("decorate_end");
	else
		ret = fputs(str, fp);

	return ret;
}


int decorate_start(FILE *fp)
{
	return fputs("START", fp);
}


void help()
{
	fprintf(stderr,
			"ttb-help:\n-h\tshow this help"
			"\n-d\tdecorate bit-file with 'START' and 'STOP-i' ASCII,\n"
			"\t  i showing how many bits in the last byte are unused.\n"
			"-s\tdecorate only the start of the file.\n"
			"-e\tdeocorate only the end of the file.\n"
			"-o\tfill unused bits at the end with Ones.\n"
			"-z\tfill unused bits at the end with Zeros (default).\n");
}


int main(int argc, char *argv[])
{
	char *outname=NULL;
	int rem=0, ar;
	FILE *fin=NULL, *fout=NULL;
	bool de=false, ds=false, zeros=true;

	while ((ar = getopt(argc, argv, "dseozh")) != -1) {
		switch (ar) {
		case 'd':
			ds = true;
			de = true;
			break;
		case 's':
			ds = true;
			de = false;
			break;
		case 'e':
			ds = false;
			de = true;
			break;
		case 'o':
			zeros = false;
			break;
		case 'z':
			zeros = true;
			break;
		case 'h':
		default:
			help();
			goto cleanup;
		}
	}

	if (optind != argc-1) {
		fprintf(stderr, "No filenames given. Reading / writing to std.\n");
		fin  = stdin;
		fout = stdout;
	} else {
		if (argc-1-optind > 0)
			fprintf(stderr, "More than one file given. Parsing only the first.\n");

		fin = fopen(argv[optind], "r");
		if (!fin) {
			perror(NULL);
			goto cleanup;
		}

		outname = prepare_filename(argv[optind]);
		if (!outname)
			goto cleanup;

		fout = fopen(outname, "wb");
		if (!fout) {
			perror(NULL);
			goto cleanup;
		}
	}

	if (ds)
		if (decorate_start(fout) == EOF) {
			perror(NULL);
			goto cleanup;
		}

	rem = parse_and_write(fin, fout, zeros);
	if (rem < 0) {
		perror(NULL);
		goto cleanup;
	}

	if (de)
		if (decorate_end(fout, rem) == EOF)
			perror("decorate_end");


cleanup:
	free(outname);
	if (fin)
		if (fclose(fin) != 0)
			perror(NULL);

	if (fout)
		if (fclose(fout) != 0)
			perror(NULL);

	return (outname && fin && fout) ? EXIT_SUCCESS : EXIT_FAILURE;
}
