/*
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *    (C) Philipp Stanner, <stanner@posteo.de>
 */

#include <stdint.h>

/* Mirrors a Byte at the line between Bit No. 4 and 5 */
uint8_t swapper(uint8_t input)
{
	short i, n;
	uint8_t choosen_bit, output = 0x00;

	/*Choose a bit with n for each run and shift it upwards using i */
	for (i=7, n=0; n<4; i-=2, n++) {
		choosen_bit = input & (1<<n);
		output |= choosen_bit<<i;
	}

	/*The same as above, but now shift the upper 4 bits downwards */
	for (i=1, n=4; n<8; i+=2, n++) {
		choosen_bit = input & (1<<n);
		output |= choosen_bit>>i;
	}

	return output;
}
